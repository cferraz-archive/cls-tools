module.exports = {
  bind$: require('./bind'),
  clone$: require('./clone'),
  compose$: require('./compose'),
  curry$: require('./curry'),
  deepEq$: require('./deep-eq'),
  extend$: require('./extend'),
  flip$: require('./flip'),
  importAll$: require('./import-all'),
  import$: require('./import'),
  in$: require('./in'),
  not$: require('./not'),
  partialize$: require('./partialize'),
  repeatArray$: require('./repeat-array'),
  repeatString$: require('./repeat-string')
}
