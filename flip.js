var curry$ = require('./curry');
module.exports = function flip$(f){
  return curry$(function (x, y) { return f(y, x); });
};
